﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace TronGame
{
	public class Player
	{
		public int X;
		public int Y;
		public int Direction; //0-up, 1-left, 2-down, 3-right
		public int BoundX;
		public int BoundY;
		public Dictionary<int, Action> moves;

		public Player() 
		{
			moves = new Dictionary<int, Action>();
			moves.Add(0, new Action(() => Y -= 1));
			moves.Add(1, new Action(() => X -= 1));
			moves.Add(2, new Action(() => Y += 1));
			moves.Add(3, new Action(() => X += 1));
		}

		public void Move()
		{
			moves[Direction]();

			if (X == BoundX)	
				X = 0;
			else if (X == -1)
				X = BoundX - 1;

			if (Y == BoundY)
				Y = 0;
			else if (Y == -1)
				Y = BoundY - 1;
		}
	}

	public class Model
	{
		public Action<List<Point>, List<Point>> Update; //p1x, p1y, p2x, p2y

		public int GridWidth { get; private set; }
		public int GridHeight { get; private set; }

		public int P1Wins;
		public int P2Wins;

		private int[,] grid;
		private Player p1;
		private Player p2;
		private int speed;
		public int State = 0; //-1-start, 0-Playing 1-P1win, 2-P2win

		public Model(int gridWidth, int gridHeight, int gameSpeed)
		{
			GridWidth = gridWidth;
			GridHeight = gridHeight;
			speed = gameSpeed;
			State = -1;
		}

		public void Start()
		{
			if (State == 0)
				return;

			State = 0;

			grid = new int[GridHeight, GridWidth]; //0-empty, 1-p1, 2-p2

			var random = new Random();
			var px1 = random.Next(GridWidth);
			var px2 = random.Next(GridWidth);
			var py1 = random.Next(GridHeight);
			var py2 = random.Next(GridHeight);
			while (px1 == px2 && py1 == py2)
			{
				px2 = random.Next(GridWidth);
				py2 = random.Next(GridHeight);
			}
			var d1 = random.Next(4);
			var d2 = random.Next(4);
			p1 = new Player() { X = px1, Y = py1, Direction = d1, BoundX = GridWidth, BoundY = GridHeight };
			p2 = new Player() { X = px2, Y = py2, Direction = d2, BoundX = GridWidth, BoundY = GridHeight };

			grid[py1, px1] = 1;
			grid[py2, px2] = 2;
		}

		public void Start(Player p1, Player p2)
		{
			if (State == 0)
				return;

			State = 0;

			grid = new int[GridHeight, GridWidth]; //0-empty, 1-p1, 2-p2

			this.p1 = p1;
			this.p2 = p2;
			grid[p1.Y, p1.X] = 1;
			grid[p2.Y, p2.X] = 2;
		}

		public void OnTick()
		{
			if (State != 0)
				return;

			var p1Points = new List<Point>();
			var p2Points = new List<Point>();

			for (var i = 0; i < speed; ++i)
			{
				p1.Move();
				p2.Move();

				p1Points.Add(new Point(p1.X, p1.Y));
				p2Points.Add(new Point(p2.X, p2.Y));

				if (grid[p1.Y, p1.X] != 0)
				{
					State = 2;
					P2Wins += 1;
					break;
				}
				
				grid[p1.Y, p1.X] = 1;

				if (grid[p2.Y, p2.X] != 0)
				{
					State = 1;
					P1Wins += 1;
					break;
				}
				
				grid[p2.Y, p2.X] = 2;
			}

			Update?.Invoke(p1Points, p2Points);
		}

		public void SetP1Direction(int direction)
		{
			if (State != 0)
				return;

			switch (direction)
			{
				case 0 when p1.Direction != 2:
				case 2 when p1.Direction != 0:
				case 1 when p1.Direction != 3:
				case 3 when p1.Direction != 1:
					p1.Direction = direction;
					break;
			}
		}

		public void SetP2Direction(int direction)
		{
			if (State != 0)
				return;

			switch (direction)
			{
				case 0 when p2.Direction != 2:
				case 2 when p2.Direction != 0:
				case 1 when p2.Direction != 3:
				case 3 when p2.Direction != 1:
					p2.Direction = direction;
					break;
			}
		}
	}
}
