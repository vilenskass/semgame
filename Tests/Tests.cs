﻿using NUnit.Framework;
using TronGame;

namespace Tests
{
	[TestFixture]
	public class Tests
	{
		[Test]
		public void P1Win()
		{
			var model = new Model(4, 4, 1);
			var p1 = new Player() { X = 0, Y = 0, Direction = 3, BoundX = 4, BoundY = 4 };
			var p2 = new Player() { X = 0, Y = 2, Direction = 0, BoundX = 4, BoundY = 4 };
		
			model.Start(p1, p2);
			model.OnTick();
			model.OnTick();

			Assert.AreEqual(model.State, 1);
		}

		[Test]
		public void P2Win()
		{
			var model = new Model(4, 4, 1);
			var p1 = new Player() { X = 0, Y = 2, Direction = 0, BoundX = 4, BoundY = 4 };
			var p2 = new Player() { X = 0, Y = 0, Direction = 3, BoundX = 4, BoundY = 4 };

			model.Start(p1, p2);
			model.OnTick();
			model.OnTick();

			Assert.AreEqual(model.State, 2);
		}

		[Test]
		public void PlayersGoThroughWallUp()
		{
			var model = new Model(4, 4, 1);
			var p1 = new Player() { X = 0, Y = 0, Direction = 0, BoundX = 4, BoundY = 4 };
			var p2 = new Player() { X = 3, Y = 0, Direction = 0, BoundX = 4, BoundY = 4 };

			model.Start(p1, p2);
			model.OnTick();

			Assert.IsTrue(p1.X == 0 && p1.Y == 3);
			Assert.IsTrue(p2.X == 3 && p2.Y == 3);
		}

		[Test]
		public void PlayersGoThroughWallLeft()
		{
			var model = new Model(4, 4, 1);
			var p1 = new Player() { X = 0, Y = 0, Direction = 1, BoundX = 4, BoundY = 4 };
			var p2 = new Player() { X = 0, Y = 3, Direction = 1, BoundX = 4, BoundY = 4 };

			model.Start(p1, p2);
			model.OnTick();

			Assert.IsTrue(p1.X == 3 && p1.Y == 0);
			Assert.IsTrue(p2.X == 3 && p2.Y == 3);
		}

		[Test]
		public void PlayersGoThroughWallDown()
		{
			var model = new Model(4, 4, 1);
			var p1 = new Player() { X = 0, Y = 3, Direction = 2, BoundX = 4, BoundY = 4 };
			var p2 = new Player() { X = 3, Y = 3, Direction = 2, BoundX = 4, BoundY = 4 };

			model.Start(p1, p2);
			model.OnTick();

			Assert.IsTrue(p1.X == 0 && p1.Y == 0);
			Assert.IsTrue(p2.X == 3 && p2.Y == 0);
		}

		[Test]
		public void PlayersGoThroughWallRight()
		{
			var model = new Model(4, 4, 1);
			var p1 = new Player() { X = 3, Y = 0, Direction = 3, BoundX = 4, BoundY = 4 };
			var p2 = new Player() { X = 3, Y = 3, Direction = 3, BoundX = 4, BoundY = 4 };

			model.Start(p1, p2);
			model.OnTick();

			Assert.IsTrue(p1.X == 0 && p1.Y == 0);
			Assert.IsTrue(p2.X == 0 && p2.Y == 3);
		}

		[Test]
		public void PlayersRotatesLeft()
		{
			var model = new Model(4, 4, 1);
			var p1 = new Player() { X = 0, Y = 0, Direction = 2, BoundX = 4, BoundY = 4 };
			var p2 = new Player() { X = 1, Y = 0, Direction = 2, BoundX = 4, BoundY = 4 };

			model.Start(p1, p2);
			model.OnTick();
			model.SetP1Direction(3);
			model.SetP2Direction(3);
			model.OnTick();

			Assert.IsTrue(p1.X == 1 && p1.Y == 1);
			Assert.IsTrue(p2.X == 2 && p2.Y == 1);
		}

		[Test]
		public void PlayersRotatesRight()
		{
			var model = new Model(4, 4, 1);
			var p1 = new Player() { X = 2, Y = 0, Direction = 2, BoundX = 4, BoundY = 4 };
			var p2 = new Player() { X = 3, Y = 0, Direction = 2, BoundX = 4, BoundY = 4 };

			model.Start(p1, p2);
			model.OnTick();
			model.SetP1Direction(1);
			model.SetP2Direction(1);
			model.OnTick();

			Assert.IsTrue(p1.X == 1 && p1.Y == 1);
			Assert.IsTrue(p2.X == 2 && p2.Y == 1);
		}

		[Test]
		public void PlayersCantRotateBackwardsUp()
		{
			var model = new Model(4, 4, 1);
			var p1 = new Player() { X = 1, Y = 2, Direction = 0, BoundX = 4, BoundY = 4 };
			var p2 = new Player() { X = 2, Y = 2, Direction = 0, BoundX = 4, BoundY = 4 };

			model.Start(p1, p2);
			model.OnTick();
			model.SetP1Direction(2);
			model.SetP2Direction(2);
			model.OnTick();

			Assert.IsTrue(p1.X == 1 && p1.Y == 0);
			Assert.IsTrue(p2.X == 2 && p2.Y == 0);
		}

		[Test]
		public void PlayersCantRotateBackwardsLeft()
		{
			var model = new Model(4, 4, 1);
			var p1 = new Player() { X = 3, Y = 0, Direction = 1, BoundX = 4, BoundY = 4 };
			var p2 = new Player() { X = 3, Y = 1, Direction = 1, BoundX = 4, BoundY = 4 };

			model.Start(p1, p2);
			model.OnTick();
			model.SetP1Direction(3);
			model.SetP2Direction(3);
			model.OnTick();

			Assert.IsTrue(p1.X == 1 && p1.Y == 0);
			Assert.IsTrue(p2.X == 1 && p2.Y == 1);
		}

		[Test]
		public void PlayersCantRotateBackwardsDown()
		{
			var model = new Model(4, 4, 1);
			var p1 = new Player() { X = 1, Y = 0, Direction = 2, BoundX = 4, BoundY = 4 };
			var p2 = new Player() { X = 2, Y = 0, Direction = 2, BoundX = 4, BoundY = 4 };

			model.Start(p1, p2);
			model.OnTick();
			model.SetP1Direction(0);
			model.SetP2Direction(0);
			model.OnTick();

			Assert.IsTrue(p1.X == 1 && p1.Y == 2);
			Assert.IsTrue(p2.X == 2 && p2.Y == 2);
		}

		[Test]
		public void PlayersCantRotateBackwardsRight()
		{
			var model = new Model(4, 4, 1);
			var p1 = new Player() { X = 0, Y = 0, Direction = 3, BoundX = 4, BoundY = 4 };
			var p2 = new Player() { X = 0, Y = 1, Direction = 3, BoundX = 4, BoundY = 4 };

			model.Start(p1, p2);
			model.OnTick();
			model.SetP1Direction(1);
			model.SetP2Direction(1);
			model.OnTick();

			Assert.IsTrue(p1.X == 2 && p1.Y == 0);
			Assert.IsTrue(p2.X == 2 && p2.Y == 1);
		}
	}
}
